import 'package:flutter/material.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:igdownloader/utils/navigator.dart';
import 'package:igdownloader/utils/constants.dart';

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {

  List<Slide> _slideList = List();

  @override
  void initState() {
    super.initState();
    _slideList.add(
      new Slide(
        title: "SHARE LINK",
        description: "Tap share link in the share menu of the post you would like to download.",
        pathImage: "assets/images/screenone.png",
        backgroundColor: Color(0xFF3C3F41),
        styleTitle: kIntroTitleTextStyle,
        styleDescription: kIntroDescTextStyle
      ),
    );
    _slideList.add(
      new Slide(
        title: "CHOOSE",
        description: "Select IGDownloader in the Share With pop up.",
        pathImage: "assets/images/screentwo.png",
        backgroundColor: Color(0xFF3C3F41),
        styleTitle: kIntroTitleTextStyle,
        styleDescription: kIntroDescTextStyle
      ),
    );
    _slideList.add(
      new Slide(
        title: "DOWNLOAD",
        description:
        "The preview of  shared photo(s)/video(s) will appear in the download list where you can download by tapping on the download icon.",
        pathImage: "assets/images/screenthree.png",
        backgroundColor: Color(0xFF3C3F41),
        styleTitle: kIntroTitleTextStyle,
        styleDescription: kIntroDescTextStyle
      ),
    );
    _slideList.add(
      new Slide(
        title: "REMOVE",
        description:
        "The unwanted items can also be removed by tapping the delete icon.",
        pathImage: "assets/images/screenfour.png",
        backgroundColor: Color(0xFF3C3F41),
        styleTitle: kIntroTitleTextStyle,
        styleDescription: kIntroDescTextStyle
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return IntroSlider(
      slides: this._slideList,
      onDonePress: this._onDonePress,
      onSkipPress: this._onSkipPress,
    );
  }

  void _onDonePress() {
    goToHomePage(context);
  }

  void _onSkipPress() {
    goToHomePage(context);
  }
}
