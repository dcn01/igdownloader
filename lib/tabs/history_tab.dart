import 'package:flutter/material.dart';
import 'package:igdownloader/db/db_helper.dart';
import 'package:igdownloader/models/mediaItem_model.dart';
import 'package:igdownloader/utils/constants.dart';

DbHelper dbHelper = DbHelper();

class HistoryTab extends StatefulWidget {
  @override
  _HistoryTabState createState() => _HistoryTabState();
}

class _HistoryTabState extends State<HistoryTab> {
  List<MediaItemModel> mediaItemList = List();

  @override
  void initState() {
    super.initState();
    getHistoryItemList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: checkState(),
    );
  }

  Widget checkState() {
    Widget historyMainWidget;
    if (mediaItemList.length <= 0) {
      historyMainWidget = emptyViewWidget();
    } else {
      historyMainWidget = historyItemWidget();
    }
    return historyMainWidget;
  }

  Widget historyItemWidget() {
    return GridView.count(
      crossAxisCount: 2,
      shrinkWrap: true,
      physics: BouncingScrollPhysics(),
      children: List.generate(mediaItemList.length, (index) {
        var mediaItem = mediaItemList[index];
        return Card(
          elevation: 8.0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
          child: GridTile(
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(4.0),
                    child: Image.network(
                      mediaItem.photoUrl,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: mediaItem.isVideo
                        ? Image.asset(
                            "assets/images/video.png",
                            width: 40.0,
                            height: 40.0,
                            color: Colors.white,
                          )
                        : null,
                  )
                ],
              ),
              footer: Align(
                alignment: Alignment.bottomRight,
                child: GestureDetector(
                  onTap: () => removeItem(int.parse(mediaItem.id)),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Image.asset(
                      "assets/images/remove.png",
                      width: 30.0,
                      height: 30.0,
                    ),
                  ),
                ),
              )),
        );
      }),
    );
  }

  void removeItem(int id) {
    dbHelper.deleteDownloadItem(id);
    getHistoryItemList();
  }

  Widget emptyViewWidget() {
    return Center(
      child: Container(
        padding: EdgeInsets.only(top: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
             "assets/images/history_empty.png",
             width: 150.0,
             height: 150.0,
           ),
            Text(
              'No History Items yet...',
              style: kLabelTextStyle,
            )
          ],
        ),
      ),
    );
  }

  void getHistoryItemList() {
    final dbFuture = dbHelper.initializeDb();
    dbFuture.then((result) {
      final downloadFuture = dbHelper.getDownloadList(downloadFinish);
      downloadFuture.then((result) {
        List<MediaItemModel> mList = List<MediaItemModel>();
        for (int i = 0; i < result.length; i++) {
          mList.add(MediaItemModel.fromObject(result[i]));
        }
        setState(() {
          mediaItemList = mList;
        });
      });
    });
  }
}
