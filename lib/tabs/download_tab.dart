import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:igdownloader/db/db_helper.dart';
import 'package:igdownloader/models/iGResponse_model.dart';
import 'package:igdownloader/models/mediaItem_model.dart';
import 'package:igdownloader/share/receive_share_state.dart';
import 'package:igdownloader/share/share.dart';
import 'package:image_downloader/image_downloader.dart';
import 'package:flutter/services.dart';
import 'package:igdownloader/utils/constants.dart';

DbHelper dbHelper = DbHelper();

class DownloadTab extends StatefulWidget {
  @override
  _DownloadTabState createState() => _DownloadTabState();
}

class _DownloadTabState extends ReceiveShareState<DownloadTab> {
  String _shared = '';
  IGResponseModel igResponseModel;
  List<MediaItemModel> mediaItemList = List();

  @override
  void initState() {
    super.initState();
    getDownloadItemList();
  }

  @override
  Widget build(BuildContext context) {
    enableShareReceiving();
    return Scaffold(
      body: checkState(),
    );
  }

  @override
  void receiveShare(Share shared) {
    setState(() {
      checkIGLink(shared.text);
    });
  }

  void checkIGLink(String shareLink) {
    try {
      var url = Uri.parse(shareLink);
      if (url.host == "www.instagram.com") {
        _shared = url.replace(queryParameters: {"__a": "1"}).toString();
        getGraphQLJson();
      } else {
        _showInfoDialog("Whoops", "Sorry, only Instagram links supported.");
      }
    } catch (e) {
      _showInfoDialog("Whoops", "Sorry, only Instagram links supported.");
      print(e.toString());
    }
  }

  void getGraphQLJson() async {
    final urlResponse = await http.get(_shared);
    if (urlResponse.statusCode == 200) {
      igResponseModel = IGResponseModel.fromJson(jsonDecode(urlResponse.body));
      mediaItemList = igResponseModel.mediaList;
      for (MediaItemModel mediaItem in mediaItemList) {
        print(mediaItem.isVideo);
        final saveResult = dbHelper.insertDownloadItem(mediaItem);
        saveResult.then((result) {
          getDownloadItemList();
        });
      }
    } else {
      _showInfoDialog("Whoops", "Owner of photo(s) has disabled sharing!");
      print(urlResponse.statusCode);
    }
  }

  void getDownloadItemList() {
    final dbFuture = dbHelper.initializeDb();
    dbFuture.then((result) {
      final downloadFuture = dbHelper.getDownloadList(downloadStart);
      downloadFuture.then((result) {
        List<MediaItemModel> mList = List<MediaItemModel>();
        for (int i = 0; i < result.length; i++) {
          mList.add(MediaItemModel.fromObject(result[i]));
        }
        setState(() {
          mediaItemList = mList;
        });
      });
    });
  }

  void _showInfoDialog(String title, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(title),
          content: new Text(message),
          actions: <Widget>[
            new FlatButton(
              child: new Text("CLOSE"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void removeItem(int id) {
    dbHelper.deleteDownloadItem(id);
    getDownloadItemList();
  }

  Widget checkState() {
    Widget downloadMainWidget;
    if (mediaItemList.length <= 0) {
      downloadMainWidget = emptyViewWidget();
    } else {
      downloadMainWidget = downloadItemWidget();
    }
    return downloadMainWidget;
  }

  Widget downloadItemWidget() {
    return GridView.count(
      crossAxisCount: 2,
      shrinkWrap: true,
      physics: BouncingScrollPhysics(),
      children: List.generate(mediaItemList.length, (index) {
        var mediaItem = mediaItemList[index];
        return Card(
          elevation: 8.0,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(4.0)),
          child: GridTile(
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(4.0),
                    child: Image.network(
                      mediaItem.photoUrl,
                      fit: BoxFit.cover,
                    ),
                  ),
                  Align(
                    alignment: Alignment.center,
                    child: mediaItem.isVideo
                        ? Image.asset(
                      "assets/images/video.png",
                      width: 40.0,
                      height: 40.0,
                      color: Colors.white,
                    )
                        : null,
                  )
                ],
              ),
              footer: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    onTap: () =>
                        removeItem(int.parse(mediaItem.id)),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.asset(
                        "assets/images/remove.png",
                        width: 30.0,
                        height: 30.0,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      String downloadLink = mediaItem.isVideo ? mediaItem.videoUrl :
                          mediaItem.photoUrl;
                      _downloadEngine(mediaItem, downloadLink);
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.asset(
                        "assets/images/download.png",
                        width: 30.0,
                        height: 30.0,
                        color: Color(0xFF6CFEDA),
                      ),
                    ),
                  ),
                ],
              )),
        );
      }),
    );
  }

  Widget emptyViewWidget() {
    return Center(
      child: Container(
        padding: EdgeInsets.only(top: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              "assets/images/download_empty.png",
              width: 150.0,
              height: 150.0,
            ),
            Text(
              'No Download Items yet...',
              style: kLabelTextStyle,
            )
          ],
        ),
      ),
    );
  }

  Future<void> _downloadEngine(MediaItemModel mediaItem, String url, {AndroidDestinationType destination, bool whenError = false}) async {
    try {
      String imageId;
      if (whenError) {
         imageId = await ImageDownloader.downloadImage(url).catchError((error){
          if (error is PlatformException) {
            if (error.code == "404") {
              print("Not found error");
            } else if (error.code == "unsupported_file") {
              print("Unsupported file errror");
            }
          }
          print(error);
        }).timeout(Duration(seconds: 20), onTimeout: (){
          print("timeout");
        });
      } else {
        if (destination == null) {
          imageId = await ImageDownloader.downloadImage(url);
        } else {
          imageId = await ImageDownloader.downloadImage(
            url,
            destination: destination
          );
        }
      }

      if (imageId == null) {
        return;
      } else {
        mediaItem.downloadStatus = downloadFinish;
        final updateResult = dbHelper.updateDownloadItem(mediaItem);
        updateResult.then((result) {
          getDownloadItemList();
        });
      }
    } on PlatformException catch (error) {
      print(error.toString());
      return;
    }

    if (!mounted) return;
  }
}
