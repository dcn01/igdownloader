import 'package:flutter/material.dart';
import 'package:igdownloader/screens/download_guide_page.dart';
import 'package:igdownloader/home_page.dart';
import 'package:igdownloader/intro_page.dart';

goToHomePage(BuildContext context) {
  _pushWidgetWithFade(context, HomePage());
}

goToIntroPage(BuildContext context) {
  _pushWidgetWithFade(context, IntroPage());
}

goToDownloadGuidePage(BuildContext context) {
  _pushWidgetWithFade(context, DownloadGuidePage());
}

_pushWidgetWithFade(BuildContext context, Widget widget) {
  Navigator.of(context).push(PageRouteBuilder(
    transitionsBuilder: (context, animation, secondaryAnimation, child) =>
        FadeTransition(opacity: animation, child: child),
    pageBuilder: (BuildContext context, Animation animation,
        Animation secondaryAnimation) {
      return widget;
    },
  ));
}