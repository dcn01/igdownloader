import 'dart:async';
import 'dart:io';

import 'package:igdownloader/models/mediaItem_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:igdownloader/utils/constants.dart';

class DbHelper {
  static final DbHelper _dbHelper = DbHelper._internal();

  // Db Version
  int dbVersion = 1;

  // Download
  String tableDownload = "download";
  String colDownloadId = "downloadId";
  String colDownloadIsVideo = "downloadIsVideo";
  String colDownloadPhotoUrl = "downloadPhotoUrl";
  String colDownloadVideoUrl = "downloadVideoUrl";
  String colDownloadStatus = "downloadStatus";

  DbHelper._internal();

  factory DbHelper() {
    return _dbHelper;
  }

  static Database _db;

  Future<Database> get db async {
    if (_db == null) {
      _db = await initializeDb();
    }
    return _db;
  }

  Future<Database> initializeDb() async {
    Directory dir = await getApplicationDocumentsDirectory();
    String path = dir.path + "download.db";
    return await openDatabase(path, version: dbVersion, onCreate: _createDb);
  }

  void _createDb(Database db, int newVersion) async {
    await db.execute(
        "CREATE TABLE $tableDownload($colDownloadId INTEGER PRIMARY KEY AUTOINCREMENT, "
            "$colDownloadIsVideo TEXT, $colDownloadPhotoUrl TEXT, $colDownloadVideoUrl TEXT ,"
            " $colDownloadStatus TEXT)");
  }

  Future<int> insertDownloadItem(MediaItemModel mediaItem) async {
    Database db = await this.db;
    return await db.insert(tableDownload, mediaItem.toMap());
  }

  Future<List<Map>> getDownloadList(String status) async {
    Database db = await this.db;
    var result = await db.rawQuery('SELECT * FROM $tableDownload WHERE $colDownloadStatus=? order by $colDownloadId desc', [status]);
    return result;
  }
  
  Future<int> updateDownloadItem(MediaItemModel mediaItem) async {
    Database db = await this.db;
    var result = await db.update(tableDownload, mediaItem.toMap(),
        where: "$colDownloadId =?", whereArgs: [mediaItem.id]);
    return result;
  }

  Future<int> deleteDownloadItem(int id) async {
    Database db = await this.db;
    return await db.rawDelete(
        'DELETE FROM $tableDownload WHERE $colDownloadId = $id');
  }

}